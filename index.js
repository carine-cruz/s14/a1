console.log("Hello World!");

let userID = `user_001`;
let firstName = `John`;
let lastName = `Smith`;
let age = '30';
let isAdmin = false;
let isMarried = true;
let gender = `M`;
let hobbies = [`Biking`,`Mountain Climbing`,`Swimming`];
let workAddress = {
	houseNumber : `32`,
	street : `Washington`,
	city : `Lincoln`,
	state : `Nebraska`,
}
let funcAdmin

/*console.log(userID);
console.log(firstName);
console.log(lastName);
console.log(age);
console.log(isAdmin);
console.log(gender);
console.log(hobbies);
console.log(workAddress);*/

function printUserInfo(fName,lName,age){
	console.log(`First Name: ${fName}`);
	console.log(`Last Name: ${lName}`);
	console.log(`Age: ${age}`);
	console.log(`Hobbies:`);
	console.log(hobbies);
	console.log(`Work Address:`);
	console.log(workAddress);
	console.log(`${fName} ${lName} is ${age} years of age.`);
}

function returnFunction(a){
	return a;
}

printUserInfo(firstName, lastName,age);

varAdmin = returnFunction(isMarried);
console.log(`The value of isMarried is: ${varAdmin}`);
